package projeto_compilador;


public class Token {
	private StringBuilder sb;
	private TipoToken tipo;
	private int codigo;
	private int linha;
	private int coluna;

	public Token(int line, int column) {
		this.sb = new StringBuilder();
		this.linha = line;
		this.coluna = column;
		this.codigo = -1;
	}

	@Override
	public String toString() {
		return "Token -> " +
				"Lexema = " + CodigoToken.getCodigo(this.codigo) +
				"; Classe = " + this.tipo +
				"; Codigo = " + this.codigo +
				"; Linha = " + this.linha +
				"; Coluna = " + this.coluna
				;
	}

	public void setLinha(int linha) {
		this.linha = linha;
	}

	public void setColuna(int coluna) {
		this.coluna = coluna;
	}

	public int getLinha() {
		return this.linha;
	}

	public int getColuna() {
		return coluna;
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int tokenCode) {
		this.codigo = tokenCode;
	}

	public String getLexema() {
		return sb.toString();
	}

	public void appendChar(char ch) {
		this.sb.append(ch);
	}

	public void limparToken() {
		sb.delete(0, sb.length());
	}

	public TipoToken getTipoToken() {
		return tipo;
	}

	public void setTipoToken(TipoToken tipo) {
		this.tipo = tipo;
	}
}
