package projeto_compilador;

public class CodigoToken {

	// palavras reservadas
	public static final int PALAVRA_MAIN = 0;
	public static final int PALAVRA_IF = 1;
	public static final int PALAVRA_ELSE = 2;
	public static final int PALAVRA_WHILE = 3;
	public static final int PALAVRA_DO = 4;
	public static final int PALAVRA_FOR = 5;
	public static final int PALAVRA_INT = 6;
	public static final int PALAVRA_FLOAT = 7;
	public static final int PALAVRA_CHAR = 8;
	
	// operadores relacionais
	public static final int OPERADOR_RELAC_MAIOR = 20;
	public static final int OPERADOR_RELAC_MENOR = 21;
	public static final int OPERADOR_RELAC_MAIOR_IGUAL = 22;
	public static final int OPERADOR_RELAC_MENOR_IGUAL = 23;
	public static final int OPERADOR_RELAC_IGUAL = 24;
	public static final int OPERADOR_RELAC_DIFERENTE = 25;
	 
	//operadores aritimeticos
	public static final int OPERADOR_ARIT_ATRIBUICAO = 30;
	public static final int OPERADOR_ARIT_ADICAO = 31;
	public static final int OPERADOR_ARIT_SUBTRACAO = 32;
	public static final int OPERADOR_ARIT_MULTIPLICACAO = 33;
	public static final int OPERADOR_ARIT_DIVISAO = 34;
	
	// caracteres especiais
	public static final int ESPECIAL_ABRE_PARENTESES = 40;
	public static final int ESPECIAL_FECHA_PARENTESES = 41;
	public static final int ESPECIAL_ABRE_CHAVES = 42;
	public static final int ESPECIAL_FECHA_CHAVES = 43;
	public static final int ESPECIAL_PONTO = 44;
	public static final int ESPECIAL_VIRGULA = 45;
	public static final int ESPECIAL_PONTO_E_VIRGULA = 46;
	public static final int ESPECIAL_ASPAS = 47;


	public static String getCodigo(int x){
		if(x == PALAVRA_MAIN) return "MAIN";
		else if (x == PALAVRA_IF) return "IF";
		else if  (x == PALAVRA_ELSE) return "ELSE";
		else if  (x == PALAVRA_WHILE) return "WHILE";
		else if  (x == PALAVRA_DO) return "DO";
		else if  (x == PALAVRA_FOR) return "FOR";
		else if  (x == PALAVRA_INT) return "INT";
		else if  (x == PALAVRA_FLOAT) return "FLOAT";
		else if  (x == PALAVRA_CHAR) return "PALAVRA CHAR";
		else if  (x == OPERADOR_RELAC_MAIOR) return "OPERADOR RELAC MAIOR";
		else if  (x == OPERADOR_RELAC_MAIOR_IGUAL) return "OPERADOR RELAC MAIOR IGUAL";
		else if  (x == OPERADOR_RELAC_MENOR_IGUAL) return "OPERADOR RELAC MENOR IGUAL";
		else if  (x == OPERADOR_RELAC_IGUAL) return "OPERADOR RELAC IGUAL";
		else if  (x == OPERADOR_RELAC_DIFERENTE) return "OPERADOR RELAC DIFERENTE";
		else if  (x == OPERADOR_ARIT_ATRIBUICAO) return "OPERADOR ARIT ATRIBUICAO";
		else if  (x == OPERADOR_ARIT_ADICAO) return "OPERADOR ARIT ADICAO";
		else if  (x == OPERADOR_ARIT_SUBTRACAO) return "OPERADOR ARIT SUBTRACAO";
		else if  (x == OPERADOR_ARIT_MULTIPLICACAO) return "OPERADOR ARIT MULTIPLICACAO";
		else if  (x == OPERADOR_ARIT_DIVISAO) return "OPERADOR ARIT DIVISAO";
		else if  (x == ESPECIAL_ABRE_PARENTESES) return "ABRE PARENTESES";
		else if  (x == ESPECIAL_FECHA_PARENTESES) return "FECHA PARENTESES";
		else if  (x == ESPECIAL_ABRE_CHAVES) return "ABRE CHAVES";
		else if  (x == ESPECIAL_FECHA_CHAVES) return "FECHA CHAVES";
		else if  (x == ESPECIAL_PONTO) return "PONTO";
		else if  (x == ESPECIAL_VIRGULA) return "VIRGULA";
		else if  (x == ESPECIAL_PONTO_E_VIRGULA) return "VIRGULA";
		else if (x == ESPECIAL_ASPAS) return "ASPAS";
		return "IDENTIFICADOR";

	}

}
