package projeto_compilador;

import java.util.HashMap;
import java.util.Map.Entry;

public class PalavrasReservadas {
	private static HashMap<Integer, String> dicionario;
	static {
		dicionario = new HashMap<Integer, String>();
		dicionario.put(CodigoToken.PALAVRA_MAIN, "main");
		dicionario.put(CodigoToken.PALAVRA_IF, "if");
		dicionario.put(CodigoToken.PALAVRA_ELSE, "else");
		dicionario.put(CodigoToken.PALAVRA_WHILE, "while");
		dicionario.put(CodigoToken.PALAVRA_DO, "do");
		dicionario.put(CodigoToken.PALAVRA_FOR, "for");
		dicionario.put(CodigoToken.PALAVRA_INT, "int");
		dicionario.put(CodigoToken.PALAVRA_FLOAT, "float");
		dicionario.put(CodigoToken.PALAVRA_CHAR, "char");

		dicionario.put(CodigoToken.OPERADOR_ARIT_ADICAO, "+");
		dicionario.put(CodigoToken.OPERADOR_ARIT_SUBTRACAO, "-");
		dicionario.put(CodigoToken.OPERADOR_ARIT_DIVISAO, "/");
		dicionario.put(CodigoToken.OPERADOR_ARIT_MULTIPLICACAO, "*");
		dicionario.put(CodigoToken.OPERADOR_ARIT_ATRIBUICAO, "=");

		dicionario.put(CodigoToken.OPERADOR_RELAC_IGUAL, "==");
		dicionario.put(CodigoToken.OPERADOR_RELAC_DIFERENTE, "!=");
		dicionario.put(CodigoToken.OPERADOR_RELAC_MAIOR, ">");
		dicionario.put(CodigoToken.OPERADOR_RELAC_MAIOR_IGUAL, ">=");
		dicionario.put(CodigoToken.OPERADOR_RELAC_MENOR, "<");
		dicionario.put(CodigoToken.OPERADOR_RELAC_MENOR_IGUAL, "<=");
	}

	public static int getCodigo(String token) {
		for (Entry<Integer, String> item : dicionario.entrySet()) {
			if (item.getValue().equals(token))
				return item.getKey();
		}
		return -1;
	}

	public static String getPalavra(int codigo) {
		return dicionario.get(codigo);
	}
}
