package projeto_compilador;

public class CompilacaoErrorException extends Exception {
	private int l,c;

	public CompilacaoErrorException(int l, int c, String m) {
		super(m);
		this.l = l;
		this.c = c;
	}
}
