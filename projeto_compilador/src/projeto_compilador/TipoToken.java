package projeto_compilador;

public enum TipoToken {
	identificador,
	operador_relacional, 
	operador_aritmético, 
	caracter_especial, 
	palavra_reservada, 
	const_int, 
	const_float, 
	const_char,
}
