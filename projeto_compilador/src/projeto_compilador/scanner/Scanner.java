package projeto_compilador.scanner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import projeto_compilador.TipoToken;
import projeto_compilador.CodigoToken;
import projeto_compilador.PalavrasReservadas;
import projeto_compilador.Token;

public class Scanner {
	private boolean isEof;
	private int linha;
	private int pos;
	private char lah; // look a head
	private int coluna;
	private char[] conteudo;

	public Scanner(String fonte) throws FileNotFoundException {
		this.isEof = false;
		this.lah = '\0';
		this.linha = 1;
		this.coluna = 1;
		this.pos = 0;
		String txtConteudo;
		try {
			txtConteudo = new String(Files.readAllBytes(Paths.get(fonte)),StandardCharsets.UTF_8);
			conteudo = txtConteudo.toCharArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readChar() throws IOException {
		if(!isEOF()) {
			int aux = conteudo[pos++];
			
			if (aux != -1) {
				this.lah = (char) aux;
				this.coluna++;
			} else {
				this.lah = '\0';
				this.isEof = true;
			}
		} else {
			this.isEof = true;
		}
	}

	private void nextChar(Token token) throws IOException {
		token.appendChar(this.lah);
		this.readChar();
	}

	public boolean isEof() {
		return this.isEof;
	}

	public Token nextToken() throws Exception {
		try {
			while (!this.isEof) {
				Token token = new Token(this.linha, this.coluna);

				if (this.pos == 0)
					this.readChar();

				if (Character.isWhitespace(this.lah)) {
					tratarEspaco();
					this.readChar();
					continue;
				}

				if (Character.isLetter(this.lah)) {
					do {
						this.nextChar(token);
					} while (Character.isLetterOrDigit(lah));
					int auxCodigo = PalavrasReservadas.getCodigo(token.getLexema());

					if (auxCodigo > -1) {
						token.setCodigo(auxCodigo);
						token.setTipoToken(TipoToken.palavra_reservada);
						token.limparToken();
					} else {
						token.setTipoToken(TipoToken.identificador);
					}
					return token;
				} else if (Character.isDigit(this.lah)) {
					token.setTipoToken(TipoToken.const_int);

					do {
						this.nextChar(token);
					} while (Character.isDigit(this.lah));

					if (this.lah == '.') {
						token.setTipoToken(TipoToken.const_float);
						this.nextChar(token);
						if (!Character.isDigit(this.lah))
							throw new LexicoErrorException(token.getLinha(), token.getColuna(),
									"Constante float mal formada.");
						do {
							this.nextChar(token);
						} while (Character.isDigit(this.lah));
					}

					return token;
				} else if (this.lah == '.') {

					this.nextChar(token);

					if (!Character.isDigit(this.lah)) {
						token.setTipoToken(TipoToken.caracter_especial);
						token.setCodigo(CodigoToken.ESPECIAL_PONTO);
						token.limparToken();
					} else {
						token.setTipoToken(TipoToken.const_float);
						do {
							this.nextChar(token);
						} while (Character.isDigit(this.lah));
					}
					return token;
				} else if (this.lah == ',') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_VIRGULA);
					token.limparToken();
					return token;
				} else if (this.lah == ';') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_PONTO_E_VIRGULA);
					token.limparToken();
					return token;
				} else if (this.lah == '(') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_ABRE_PARENTESES);
					token.limparToken();
					return token;
				} else if (this.lah == ')') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_FECHA_PARENTESES);
					token.limparToken();
					return token;
				} else if (this.lah == '{') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_ABRE_CHAVES);
					token.limparToken();
					return token;
				} else if (this.lah == '}') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.caracter_especial);
					token.setCodigo(CodigoToken.ESPECIAL_FECHA_CHAVES);
					token.limparToken();
					return token;
				} else if (this.lah == '\'') {
					this.nextChar(token);
					if (Character.isLetterOrDigit(this.lah)) {
						this.nextChar(token);
						if (this.lah == '\'') {
							token.setTipoToken(TipoToken.const_char);
							this.nextChar(token);
						} else {
							throw new LexicoErrorException(token.getLinha(), token.getColuna(),
									"Constante char mal formada.");
						}
					} else {
						token.setTipoToken(TipoToken.caracter_especial);
						token.setCodigo(CodigoToken.ESPECIAL_ASPAS);
						token.limparToken();
					}
					return token;
				} else if (this.lah == '<') {
					token.setTipoToken(TipoToken.operador_relacional);
					this.nextChar(token);
					if (this.lah == '=') {
						this.nextChar(token);
						token.setCodigo(CodigoToken.OPERADOR_RELAC_MENOR_IGUAL);
						token.limparToken();
					} else {
						token.setCodigo(CodigoToken.OPERADOR_RELAC_MENOR);
						token.limparToken();
					}
					return token;
				} else if (this.lah == '>') {
					token.setTipoToken(TipoToken.operador_relacional);
					this.nextChar(token);
					if (this.lah == '=') {
						this.nextChar(token);
						token.setCodigo(CodigoToken.OPERADOR_RELAC_MAIOR_IGUAL);
						token.limparToken();
					} else {
						token.setCodigo(CodigoToken.OPERADOR_RELAC_MAIOR);
						token.limparToken();
					}
					return token;
				} else if (this.lah == '=') {
					this.nextChar(token);
					if (this.lah == '=') {
						this.nextChar(token);
						token.setTipoToken(TipoToken.operador_relacional);
						token.setCodigo(CodigoToken.OPERADOR_RELAC_IGUAL);
						token.limparToken();
					} else {
						token.setTipoToken(TipoToken.operador_aritmético);
						token.setCodigo(CodigoToken.OPERADOR_ARIT_ATRIBUICAO);
						token.limparToken();
					}
					return token;
				} else if (this.lah == '!') {
					this.nextChar(token);
					if (this.lah == '=') {
						this.nextChar(token);
						token.setTipoToken(TipoToken.operador_relacional);
						token.setCodigo(CodigoToken.OPERADOR_RELAC_DIFERENTE);
						token.limparToken();
					} else {
						throw new LexicoErrorException(token.getLinha(), token.getColuna(),
								"Operador relacional invalido, esperado \"!=\".");
					}
					return token;
				} else if (this.lah == '+') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.operador_aritmético);
					token.setCodigo(CodigoToken.OPERADOR_ARIT_ADICAO);
					token.limparToken();
					return token;
				} else if (this.lah == '-') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.operador_aritmético);
					token.setCodigo(CodigoToken.OPERADOR_ARIT_SUBTRACAO);
					token.limparToken();
					return token;
				} else if (this.lah == '*') {
					this.nextChar(token);
					token.setTipoToken(TipoToken.operador_aritmético);
					token.setCodigo(CodigoToken.OPERADOR_ARIT_MULTIPLICACAO);
					token.limparToken();
					return token;
				} else if (this.lah == '/') {
					this.nextChar(token);
					if (this.lah == '/') {
						return readComment2(token);// pode ser que mude
					} else if (this.lah == '*') {
						return readComment(token);
					} else {
						token.setTipoToken(TipoToken.operador_aritmético);
						token.setCodigo(CodigoToken.OPERADOR_ARIT_DIVISAO);
						token.limparToken();
						return token;
					}
				} else {
					throw new LexicoErrorException(linha, coluna,
							"Caractere especificado nao eh reconhecido pela linguagem.");
				}
			}
			return null;
		} finally {
		}
	}
	
	private boolean isDigit(char c) {
		return c >= '0' && c <= '9';
	}
	
	private boolean isChar(char c) {
		return (c >= 'a' && c <= 'z') || (c>='A' && c <= 'Z');
	}
	
	private boolean isOperator(char c) {
		return c == '>' || c == '<' || c == '=' || c == '!' || c == '+' || c == '-' || c == '*' || c == '/';
	}
	private boolean isSpace(char c) {
		if (c == '\n' || c== '\r') {
			linha++;
			coluna = 0;
		}
		return c == ' ' || c == '\t' || c == '\n' || c == '\r'; 
	}
	
	private char nextChar() {
		if (isEOF()) {
			return '\0';
		}
		return conteudo[pos++];
	}
	public boolean isEOF() {
		return pos >= conteudo.length;
	}
	
	public int pos() {
		return pos;
	}
	

	public int conteudo() {
		return conteudo.length;
	}
	
    private void back() {
    	pos--;
    }
    
    private boolean isEOF(char c) {
    	return c == '\0';
    }

	private Token readComment2(Token token) throws Exception {
		token.limparToken();
		this.nextChar(token);
		token.limparToken();
		this.nextToken();
		return this.nextToken();
	}

	private Token readComment(Token token) throws Exception {
		do {
			if (Character.isWhitespace(this.lah)) {
				tratarEspaco();
			}
			this.nextChar(token);
		} while (this.lah != '*' && !this.isEof);

		if (this.isEof) {
			throw new LexicoErrorException(token.getLinha(), token.getColuna(), "Fim do comentario esperado.");
		}

		do {
			this.nextChar(token);
		} while (this.lah == '*');

		if (this.isEof) {
			throw new LexicoErrorException(token.getLinha(), token.getColuna(), "Fim do coment�rio esperado.");
		}

		if (this.lah != '/') {
			return this.readComment(token);
		} else {
			this.nextChar(token);
			token.limparToken();
			return this.nextToken();
		}
	}

	private void tratarEspaco() {
		if (this.lah == '\t') {
			this.coluna += 3;
		} else if (this.lah == '\n') {
			this.coluna = 0;
			this.linha++;
		}
	}

}
