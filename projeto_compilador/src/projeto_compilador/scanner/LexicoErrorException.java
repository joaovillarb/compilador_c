package projeto_compilador.scanner;

import projeto_compilador.CompilacaoErrorException;

public class LexicoErrorException extends CompilacaoErrorException {
	public LexicoErrorException(int l, int c, String m) {
		super(l, c, String.format("[Erro lexico] linha: %d, coluna: %d\n%s", l, c, m));
	}	
}
