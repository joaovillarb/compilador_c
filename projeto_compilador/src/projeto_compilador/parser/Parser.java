package projeto_compilador.parser;

import projeto_compilador.scanner.Scanner;

import projeto_compilador.Token;

public class Parser {

	private Scanner scanner;
	private Token token;

	public Parser(Scanner scanner) throws Exception {
		this.scanner = scanner;
	}

	public void init() throws Exception {
		if(!scanner.isEOF()) {			
			nextToken();
			init();
		}
	}

	private void nextToken() throws Exception {
		if (this.token != null) System.out.println(this.token.toString());
		this.token = scanner.nextToken();
	}


}
